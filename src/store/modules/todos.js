import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';

axios.defaults.baseURL = 'https://vue-todo-7153b.firebaseio.com';

Vue.use(VueAxios, axios);

// initital state
const state = {
  disabledAdd: false,
  todos: [],
};

const getters = {
  // eslint-disable-next-line
  getTodos: state => (state.todos),

  // eslint-disable-next-line
  getAddState: state => (state.disabledAdd),
};

const mutations = {
  // eslint-disable-next-line
  getTodos: (state, todos) => {
    if (todos.length) {
      state.todos = todos.sort((a, b) => (b.id - a.id));
    }
  },

  // eslint-disable-next-line
  addTodo: (state, todo) => {
    state.todos.unshift(todo);
  },

  // eslint-disable-next-line
  removeTodo: (state, todo) => {
    const index = state.todos.indexOf(todo);

    state.todos.splice(index, 1);
  },

  // eslint-disable-next-line
  toggleCheck: (state, todo) => {
    state.todos.find(x => x.id === todo.id).checked = todo.checked;
  },

  // eslint-disable-next-line
  disabledAdd: (state) => {
    state.disabledAdd = true;
  },

  // eslint-disable-next-line
  enabledAdd: (state) => {
    state.disabledAdd = false;
  },
};

const actions = {
  getTodos: (context) => {
    context.commit('disabledAdd');

    Vue.axios
      .get('/todos.json')
      .then((res) => {
        if (res.status === 200 && res.data) {
          const todos = Object.keys(res.data).map(key => ({
            ...res.data[key],
            firebaseId: key,
          }));

          context.commit('getTodos', todos);
        }
      })
      .finally(() => {
        context.commit('enabledAdd');
      });
  },

  addTodo: (context, todo) => {
    context.commit('disabledAdd');

    Vue.axios
      .post('/todos.json', todo)
      .then((res) => {
        if (res.status === 200) {
          context.commit('addTodo', {
            ...todo,
            firebaseId: res.data.name,
          });
        }
      })
      .finally(() => {
        context.commit('enabledAdd');
      });
  },

  removeTodo: (context, todo) => {
    context.commit('disabledAdd');

    Vue.axios
      .delete(`/todos/${todo.firebaseId}.json`)
      .then((res) => {
        if (res.status === 200) {
          context.commit('removeTodo', todo);
        }
      })
      .finally(() => {
        context.commit('enabledAdd');
      });
  },

  toggleCheck: (context, todo) => {
    context.commit('disabledAdd');

    Vue.axios
      .patch(`/todos/${todo.firebaseId}.json`, {
        checked: todo.checked,
      })
      .then((res) => {
        if (res.status === 200) {
          context.commit('toggleCheck', todo);
        }
      })
      .finally(() => {
        context.commit('enabledAdd');
      });
  },
};

export default {
  state,
  getters,
  mutations,
  actions,
};
